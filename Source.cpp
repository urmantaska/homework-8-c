#include <iostream>
#include <string>

using namespace std;
class Animal
{
public:
    virtual void Voice() {};
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meow" << endl;
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Woof" << endl;
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        cout << "Moo" << endl;
    }
};

int main()
{
	    Animal* animals[3];
	    animals[0] = new Cat();
	    animals[1] = new Dog();
		animals[2] = new Cow();
	
	    for (Animal* a : animals)
	        a->Voice();
}